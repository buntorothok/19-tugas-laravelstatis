<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //
    public function index()
    {
        return view('register');
    }

    public function submit(Request $request)
    {
        $firstname = $request["firstname"];
        $lastname = $request["lastname"];
        if($firstname == "" OR $lastname == "")
        {
            echo "Firstname dan Lastname tidak boleh kosong masbro. <a href='/register'>Registrasi</a> ulang ya.";
        }else
        {
            return view('welcome', compact('firstname', 'lastname'));
        }
    }
}
