<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Belajar Laravel Bareng Sanbercode</title>
</head>
<body>
    <h2>Buat Account Baru!</h2>
    <h3>Sign Up Form</h3>

    <form action="/register" method="post">
        @csrf
        <label for="firstname">Firstname:</label><br>
        <input type="text" placeholder="Firstname" id="firstname" name="firstname"><br><br>
        <label for="lastname">Last name:</label><br>
        <input type="text" placeholder="Last Name" id="lastname" name="lastname"><br><br>
        <label for="gender">Gender:</label><br>
        <input type="radio" name="gender" value="1">Male<br>
        <input type="radio" name="gender" value="2">Female<br>
        <span><em>no other gender!</em></span><br><br>
        <label>Nationality<label><br>
        <select name="nationality">
        <option value="id">Indonesian</option>
        <option value="my">Malaysian</option>
        <option value="au">Australian</option>
        <option value="other">Other</option>
        </select><br><br>
        <label>Language Spoken<label><br>
        <input type="checkbox" name="lang" value="id">Bahasa Indonesia<br>
        <input type="checkbox" name="lang" value="en">English<br>
        <input type="checkbox" name="lang" value="other">Other<br><br>
        <label>Bio:</label><br>
        <textarea name="bio" cols="50" rows="7"></textarea><br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>