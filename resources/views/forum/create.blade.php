@extends('adminlte.master')

@section('content')
    <div class="row">
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Buat Pertanyaan Baru</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" action="/pertanyaan" method="POST" name="fCreatePertanyaan" id="fCreatePertanyaan">
                @csrf
              <div class="card-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Judul Pertanyaan</label>
                  <input type="text" class="form-control" id="judul" name="judul" value="{{ old('judul', '') }}" placeholder="Judul pertanyaan ...">
                  @error('judul')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                </div>
                <div class="form-group">
                    <label>Isi Pertanyaan</label>
                    <textarea class="form-control" id="isi" name="isi" rows="3" placeholder="Isi pertanyaan ...">{{ old('isi', '') }}</textarea>
                    @error('isi')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <button type="submit" name="create" class="btn btn-success">Simpan</button>
              </div>
            </form>
          </div>

        </div>
      </div>
@endsection