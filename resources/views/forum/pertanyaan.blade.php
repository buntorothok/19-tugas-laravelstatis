@extends('adminlte.master')

@section('content')

        <div class="card">
            <div class="card-header">
              <h3 class="card-title">Daftar Pertanyaan</h3>

              <div class="card-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                  <div class="input-group-append">
                    <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                  </div>
                  
                </div>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive">                
              <a href="/pertanyaan/create" class="btn btn-success"><i class="fa fa-plus"></i>&nbsp;&nbsp;Tanya</a>
              @if(session('success'))
                  <div class="alert alert-success mt-1">
                    {{ session('success')}}
                  </div>
                  @endif
              <table class="table table-hover" id="tabelPertanyaan">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Judul</th>
                    <th>Isi</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                    @forelse ($questions as $key => $question)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $question->judul }}</td>
                        <td>{{ $question->isi }}</td>
                        <td style="display: flex">
                            <a href="/pertanyaan/{{$question->id}}" class="btn btn-info btn-sm ml-1">
                              <i class="nav-icon fas fa-info"></i> Details
                            </a>
                            <a href="/pertanyaan/{{$question->id}}/edit" class="btn btn-warning btn-sm ml-1" alt="Edit">
                              <i class="nav-icon fas fa-edit"></i> Edit
                            </a>
                            <form action="/pertanyaan/{{$question->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger btn-sm ml-1"><i class="fa fa-trash"></i> Delete</button>
                            </form>
                        </td>
                      </tr>
                    @empty
                    <tr>
                        <td colspan="4">Belum ada pertanyaan</td>
                      </tr>
                    @endforelse
                  
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          
@endsection
