@extends('adminlte.master')

@section('content')
<div class="card">
    <div class="card-header">
      <h3 class="card-title">{{$question->judul}}</h3>
      <form action="/pertanyaan/{{$question->id}}" method="POST" class="float-right">
        @csrf
        @method('DELETE')
        <button class="btn btn-danger btn-sm ml-1"><i class="fa fa-trash"></i> Delete</button>
      </form>
      <a href="/pertanyaan/{{$question->id}}/edit" class="btn btn-warning btn-sm float-right ml-1">
        <i class="nav-icon fas fa-edit"></i> Edit
      </a>
      <a href="/pertanyaan/create" class="btn btn-success btn-sm float-right ml-1">
        <i class="fa fa-plus"></i> Tanya
      </a>
    </div>
    <!-- /.card-header -->
    <div class="card-body table-responsive">
      <p>{{$question->isi}}</p>        
    </div>
    <!-- /.card-body -->
  </div>    
@endsection